import 'package:flutter/material.dart';
import 'package:riego_app/inicio.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Riego',
      home: Inicio(),
    );
  }
}