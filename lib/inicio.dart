import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Inicio extends StatelessWidget {

  final databaseReference = Firestore.instance;
  
  
  @override
  Widget build(BuildContext context) {
    getData();
    return Scaffold(
      appBar: AppBar(
               backgroundColor: Colors.green,
               title: const Text('Control de Riego'),
              leading: Padding(
                padding: EdgeInsets.only(left: 12),
                child: CircleAvatar(backgroundImage: NetworkImage("https://dibujosfaciles-dehacer.com/wp-content/uploads/2019/10/dibujos-de-arboles.jpg"),),
              ),
      ),
      body: StreamBuilder(
        stream: databaseReference.collection("RedRiego").snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData) return new CircularProgressIndicator();
        return new ListView(
          children: snapshot.data.documents.map((document) {
            return new ListTile(
              title: new Text( (document['riego'] == 'SI'? 'Regando' : 'En espera')),
              subtitle: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Humedad:' + document['HumedadSuelo'] + '%'),
                  Text('Luz:' + document['Luminosidad'] + '%'),
                  Text('Temperatura:' + document['temperatura'] + '°C'),
                ],
              ),
              leading: CircleAvatar(backgroundImage: NetworkImage(document['riego'] == 'SI'? 'https://image.freepik.com/vector-gratis/vector-regando-plantas_23-2147497936.jpg' : 'https://img2.freepng.es/20190405/zoi/kisspng-cactus-clip-art-succulent-plant-image-drawing-cactus-planta-marron-verde-kawaii-familia-random-l-5ca6d3c0d9a2d5.3555659515544370568915.jpg'),),
            );
          }).toList(),
        );
      },
      ),
    );
  }

  void getData() {
    final databaseReference = Firestore.instance;
  databaseReference
      .collection("RedRiego")
      .getDocuments()
      .then((QuerySnapshot snapshot) {
    snapshot.documents.forEach((f) => print('${f.data}}'));
  });
}
}